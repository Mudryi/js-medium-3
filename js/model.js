export default class Model {
    constructor(storage) {
        this.storage = storage;
        this.tempArray = [];
    }

    getData(callback, isRemoteRequest) {
        if(isRemoteRequest && this.tempArray.length > 0) {
            callback(this.tempArray);
        } else {
            this.tempArray = [];
            this.storage.getAll(callback);
        }
    }

    saveData(data, callback) {
        let self = this;

        function saveCallback(receivedData) {
            let currentData = receivedData.length;

            for (let i = 0; i < data.length; i++) {
                data[i]["id"] = currentData + i;
                receivedData.push(data[i]);
            }

            self.storage.updateData(receivedData, callback);
        }
        this.tempArray = [];
        this.storage.getAll(saveCallback);
    }

    replaceData(data, callback) {
        this.tempArray = [];
        this.storage.updateData(data, callback);
    }

    removeData(removableRowsId, callback) {
        let self = this;
        removableRowsId.sort();

        function removeCallback(receivedData) {
            let newData = receivedData;

            for (let i = 0; i < receivedData.length; i++) {
                for (let j = 0; j < removableRowsId.length; j++) {
                    if (receivedData[i]["id"] == removableRowsId[j]) {
                        newData.splice(i, 1);
                    }
                }
            }

            for(let i = 0; i < newData.length; i++) {
                newData[i]["id"] = i;
            }

            self.storage.updateData(newData, callback);
        }
        this.tempArray = [];
        this.storage.getAll(removeCallback);
    }

    findAccording(field, inputData, currentPage, rowsPerPage, callback) {
        let regex = new RegExp("^.*" + inputData.toLowerCase() + ".*$");

        function filterCallback(receivedData) {
            let currentData = receivedData.length;
            let filteredData = [];

            for (let i = currentPage*rowsPerPage; i < (currentPage + 1)*rowsPerPage; i++) {
                if (i < currentData) {
                    if (regex.test(receivedData[i][field].toLowerCase())) {
                        filteredData.push(receivedData[i]);
                    }
                } else { break; }
            }

            callback(filteredData, currentData, rowsPerPage, true);
        }

        this.tempArray.length > 0 ? filterCallback(this.tempArray) : this.storage.getAll(filterCallback);
    }

    removeAllData(callback) {
        this.tempArray = [];
        this.storage.updateData([], callback);
    }

    sortDataByField(sortField, sortDirection, callback) {
        let self = this;

        function sortCallback(data) {
            data = data.sort(function(a, b){
                let keyA = a[sortField],
                    keyB = b[sortField];

                if(sortField === "name") {
                    keyA = keyA.toLowerCase();
                    keyB = keyB.toLowerCase();
                }

                if (sortDirection === "asc") {
                    if(keyA < keyB) return -1;
                    if(keyA > keyB) return 1;
                } else {
                    if(keyA < keyB) return 1;
                    if(keyA > keyB) return -1;
                }

                return 0;
            });

            self.tempArray = data;
            callback(data);
        }

        this.storage.getAll(sortCallback);
    }

    dragShiftData(startElementId, endElementId, callback) {
        let self = this;

        if(this.tempArray.length > 0) {
            shiftData(this.tempArray);
        } else {
            this.storage.getAll(shiftData);
        }

        function shiftData(data) {
            let startIdPosition;
            let endIdPosition;
            let tempData;

            for (let i = 0; i < data.length; i++) {
                if (!startIdPosition || !endIdPosition) {
                    if (data[i]["id"] == startElementId) {
                        startIdPosition = i;
                    }
                    if (data[i]["id"] == endElementId) {
                        endIdPosition = i;
                    }
                } else {
                    break;
                }
            }

            tempData = data[startIdPosition];
            data[startIdPosition] = data[endIdPosition];
            data[endIdPosition] = tempData;
            self.tempArray = data;
            callback(data);
        }
    }
}
