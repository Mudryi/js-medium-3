export default class Storage {
    constructor(name) {
        this.name = name;
        this.getLocalStorage = () => {
            return JSON.parse(localStorage.getItem(name));
        };
    }

    getAll(callback) {
        let self = this;

        let promise = new Promise(
            function(resolve, reject) {
                /* Emulation server side */
                window.setTimeout(function () {
                    resolve(self.getLocalStorage());
                }, 0);
            }
        );

        promise.then(receivedData => {
            receivedData = receivedData || [];
            callback(receivedData);
        });
    }

    updateData(data, callback) {
        window.localStorage.setItem(this.name, JSON.stringify(data));

        if(callback) {
            callback(data);
        }
    }
}
