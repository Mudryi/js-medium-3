require('babelify-es6-polyfill');

import Controller from './controller';
import Model from './model';
import View from './view';
import Store from './storage';

const storage = new Store('table-editor');
const model = new Model(storage);
const view = new View();
const controller = new Controller(model, view);

window.onload = controller.initApp.bind(controller);
