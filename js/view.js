export default class View {
    constructor() {
        this.hidingClass = "collapse";
        this.tableRowsWrap = ".table tbody";
        this.addingRowBtn = ".adding-btn";
        this.generateDemoBtn = ".demo-btn";
        this.addingBtnsWrap = ".addition-buttons";
        this.newRowForm = ".new-row-data";
        this.deleteRowSelector = ".delete-row";
        this.deleteRowBtn = ".delete-btn";
        this.idCell = ".id-cell";
        this.clearBtn = ".clear-btn";
        this.importExportBtn = ".import-export-btn";
        this.importBtn = ".import-btn";
        this.exportDataField = ".import-export-data";
        this.filterDataInput = "#filter-by-name";
        this.paginationWrap = ".pagination";
        this.tableHeadingsWrap = ".table-headings";
    }

    bind(event, handler) {
        let self = this;

        if (event === "createTable") {
            handler();
        } else if (event === "addNewRow") {
            self.$(self.addingRowBtn).addEventListener("click", function () {
                self.$(self.addingBtnsWrap).classList.toggle(self.hidingClass);
            });

            self.$(self.newRowForm).addEventListener("submit", function (e) {
                e.preventDefault();
                handler(self._formSerialize(this));
                return false;
            });
        } else if (event === "generateDemoData") {
            self.$(self.generateDemoBtn).addEventListener("click", () => { handler() });
        } else if(event === "removeCheckedRows") {
            self.$(self.deleteRowBtn).addEventListener("click", function () {
                let selectedRows = self.$(self.deleteRowSelector + " input:checked");

                if (selectedRows) {
                    let removableRowsId = [];

                    if (!selectedRows.length) {
                        selectedRows = [selectedRows];
                    }

                    let isInputWrapElement;
                    let currentElement;

                    for (let i = 0; i < selectedRows.length; i++) {
                        isInputWrapElement = true;
                        currentElement = selectedRows[i];

                        while (isInputWrapElement && (currentElement.nodeName !== "FORM")) {
                            if (currentElement.nodeName === "TR") {
                                isInputWrapElement = false;
                                removableRowsId.push(currentElement.querySelector(self.idCell).innerHTML);
                            } else {
                                currentElement = currentElement.parentElement;
                            }
                        }
                    }
                    handler(removableRowsId);
                }
            });
        } else if(event === "removeAllRows") {
            self.$(self.clearBtn).addEventListener("click", () => { handler() });
        } else if(event === "exportData") {
            self.$(self.importExportBtn).addEventListener("click", () => {
                if(!self.$(self.exportDataField).classList.toggle(self.hidingClass)) {
                    handler();
                }
            });
        } else if (event === "importData"){
            self.$(self.importBtn).addEventListener("click", () => {
                try {
                    handler(JSON.parse(self.$(self.exportDataField).querySelector("textarea").value))
                }
                catch (e) {
                    alert("No valid data for importing!")
                }
            });
        } else if (event === "filterData") {
            self.$(self.filterDataInput).addEventListener("input", (e) => { handler(e.target.value) });
        } else if(event === "paginateData") {
            let paginationWrap = self.$(self.paginationWrap);
            paginationWrap.addEventListener("click", (e) => {
                let currentNode = e.target;

                while (!currentNode.isEqualNode(paginationWrap)) {
                    if (currentNode.classList.contains("btn")) {
                        handler(currentNode.innerText);
                        break;
                    } else {
                        currentNode = currentNode.parentNode;
                    }
                }
            })
        } else if(event === "sortData") {
            let tableHeadingsWrap = self.$(self.tableHeadingsWrap);
            tableHeadingsWrap.addEventListener("click", (e) => {
                let currentNode = e.target;
                let sortDirection;

                while (!currentNode.isEqualNode(tableHeadingsWrap)) {
                    if (currentNode.classList.contains("sort-cell")) {
                        let currentChild = currentNode.children[0];

                        if (currentNode.dataset.sortDirect === "asc") {
                            sortDirection = "desc";
                            currentChild.classList.remove("fa-arrow-down");
                            currentChild.classList.add("fa-arrow-up");
                        } else {
                            sortDirection = "asc";
                            currentChild.classList.remove("fa-arrow-up");
                            currentChild.classList.add("fa-arrow-down");
                        }

                        this._removeSortingHighLight();

                        currentNode.style.color = "#4CAF50";
                        currentNode.dataset.sortDirect = sortDirection;
                        handler(currentNode.dataset.sort, sortDirection);
                        break;
                    } else {
                        currentNode = currentNode.parentElement;
                    }
                }
            })
        } else if(event === "dragRow") {
            let elements = document.querySelectorAll(".rows-wrap tr");
            let startElement;
            let endElement;

            for(let i = 0; i < elements.length; i++) {
                elements[i].addEventListener("dragstart", function (e) {
                    /* mozilla and IE fix */
                    try {
                        e.dataTransfer.effectAllowed = 'move';
                        e.dataTransfer.setData('text/html', this.innerHTML);
                    } catch(e){}

                    startElement = this;
                    this.style.background = "#FFF0ED";
                });

                elements[i].addEventListener("dragenter", function (e) {
                    if(this !== startElement) {
                        this.style.opacity = ".2";
                    }

                    e.preventDefault();
                });

                elements[i].addEventListener("dragover", function (e) {
                    e.preventDefault();
                });

                elements[i].addEventListener("dragleave", function () {
                    this.style.opacity = "1";
                });

                elements[i].addEventListener("drop", function (e) {
                    e.preventDefault();
                    endElement = this;
                    startElement.style.background = "";
                    this.style.opacity = "1";

                    if(this !== startElement) {
                        handler(startElement.querySelector(".id-cell").innerText, endElement.querySelector(".id-cell").innerText);
                    }
                });
            }
        }
    }

    render(data, page, rowsPerPage, isFiltering) {
        let self = this;
        let rowsHtml = "";

        let paginatedData = this.paginateData(data, page, rowsPerPage);

        if(!isFiltering) {
            data = paginatedData.data;
        }

        data.forEach(function (value) {
            rowsHtml += self._generateRowHtml(value);
        });

        self.$(self.tableRowsWrap).innerHTML = rowsHtml;

        if(!isFiltering) {
            self.$(self.paginationWrap).innerHTML = paginatedData.paginationHtml;
        }
    };

    exportData(data) {
        this.$(this.exportDataField).querySelector("textarea").value = JSON.stringify(data);
    }

    paginateData(data, page, elementsPerPage) {
        let elementsCount = data.length;
        let pagesCount = Math.ceil(elementsCount/elementsPerPage);
        let paginatedData = [];

        for (let i = 0; i < elementsPerPage; i++) {
            if (page * elementsPerPage + i < data.length) {
                paginatedData.push(data[page * elementsPerPage + i]);
            } else {
                break;
            }
        }

        return {data: paginatedData, paginationHtml: this._generatePaginationBtns(pagesCount, page)};
    }

    viewErrorFields(errorFields) {
        let currentInput;
        for (let i = 0; i < errorFields.length; i++) {
            currentInput = document.getElementsByName(errorFields[i])[0];
            currentInput.classList.add("is-invalid");
            currentInput.addEventListener("focus", function () {
                this.classList.remove("is-invalid");
            });
        }
    }

    _generateRowHtml(rowData) {
        return `<tr draggable="true">
                    <td class="id-cell">${rowData.id}</td>
                    <td>${rowData.name}</td>
                    <td>${rowData.quantity}</td>
                    <td>${rowData.availability}</td>
                    <td class="delete-row"><input type="checkbox"></td>
                </tr>`;
    }

    _generatePaginationBtns(pagesCount, current) {
        let paginationElements = "";
        let pageClass = "btn-link";
        let currentPageClass = "btn-primary";
        let  buttonElement;

        for (let i = 0; i < pagesCount; i++) {
            buttonElement = document.createElement("BUTTON");
            buttonElement.innerHTML = (i+1).toString();

            if(i !== current) {
                /* Two lines for solving IE11 bug */
                buttonElement.classList.add("btn");
                buttonElement.classList.add(pageClass);
            } else {
                /* Two lines for solving IE11 bug */
                buttonElement.classList.add("btn");
                buttonElement.classList.add(currentPageClass);
            }

            paginationElements += buttonElement.outerHTML;
        }
        return paginationElements;
    }

    _formSerialize(formElement) {
        if (!formElement || formElement.nodeName !== "FORM") {
            return false;
        }

        let obj = {};

        for (let i = formElement.elements.length - 1; i >= 0; i = i - 1) {
            if (formElement.elements[i].name === "") {
                continue;
            }

            switch (formElement.elements[i].nodeName) {
                case 'INPUT':
                    switch (formElement.elements[i].type) {
                        case 'text':
                        case 'hidden':
                        case 'password':
                        case 'button':
                        case 'reset':
                        case 'submit':
                            obj[formElement.elements[i].name] = formElement.elements[i].value;
                            break;
                        case 'checkbox':
                        case 'radio':
                            formElement.elements[i].checked ? obj[formElement.elements[i].name] = "yes" : obj[formElement.elements[i].name] = "no";
                            break;
                        case 'file':
                            break;
                    }
                    break;
                case 'TEXTAREA':
                    obj[formElement.elements[i].name] = formElement.elements[i].value;
                    break;
                case 'SELECT':
                    switch (formElement.elements[i].type) {
                        case 'select-one':
                            obj[formElement.elements[i].name] = formElement.elements[i].value;
                            break;
                        case 'select-multiple':
                            for (let j = formElement.elements[i].options.length - 1; j >= 0; j = j - 1) {
                                if (formElement.elements[i].options[j].selected) {
                                    obj[formElement.elements[i].name] = formElement.elements[i].options[j].value;
                                }
                            }
                            break;
                    }
                    break;
                case 'BUTTON':
                    switch (formElement.elements[i].type) {
                        case 'reset':
                        case 'submit':
                        case 'button':
                            obj[formElement.elements[i].name] = formElement.elements[i].value;
                            break;
                    }
                    break;
            }
        }
        return obj;
    }

    _removeSortingHighLight() {
        let sortElements = this.$(this.tableHeadingsWrap).querySelectorAll(".sort-cell");

        for (let i = 0; i < sortElements.length; i++) {
            sortElements[i].style.color = "";
        }
    }

    $(selector) {
        let elements = document.querySelectorAll(selector);

        if(elements.length > 1) {
            return elements;
        } else {
            return elements[0];
        }
    }
}
