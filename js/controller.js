export default class Controller {
    constructor(model, view) {
        this.model = model;
        this.view = view;
        this.fieldRules = {name: ["not-empty"], quantity: ["int"], availability: ["bool"]};
        this.randomNameCharacters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        this.page = 0;
        this.rowsPerPage = 5;
    }

    tableCreating() {
        let self = this;

        this.model.getData((data) => {self.view.render(data, self.page, self.rowsPerPage); self._setDragEvents();});
    }

    addingRow(rowData) {
        let self = this;
        let validData = this._validateData(rowData);
        self.page = 0;

        validData.length > 0 ? this.view.viewErrorFields(validData) : this.model.saveData([rowData], (data) => {self.view.render(data, self.page, self.rowsPerPage); self._setDragEvents(); self.view._removeSortingHighLight();});
    }

    generateData() {
        let self = this;
        let generatedData = [];
        let fieldsCount = this._getRandomValue(1, 10);
        self.page = 0;

        for (let i = 1; i < fieldsCount; i++) {
            let temp = {};

            temp["availability"] = (temp["quantity"] === 0 ? "no" : (this._getRandomValue(0, 1) === 0 ? "no" : "yes"));
            temp["quantity"] = this._getRandomValue(0, 1000);
            temp["name"] = this._generateName(this.randomNameCharacters);

            generatedData.push(temp);
        }

        this.model.saveData(generatedData, (data) => {self.view.render(data, self.page, self.rowsPerPage); self._setDragEvents(); self.view._removeSortingHighLight();});
    }

    removeRows(removableRowsId) {
        let self = this;
        self.page = 0;

        this.model.removeData(removableRowsId, (data) => {self.view.render(data, self.page, self.rowsPerPage); self._setDragEvents(); self.view._removeSortingHighLight();});
    }

    removeAllRows() {
        let self = this;
        self.page = 0;

        this.model.removeAllData((data) => {self.view.render(data, self.page, self.rowsPerPage); self.view._removeSortingHighLight();});
    }

    exportData() {
        let self = this;
        this.model.getData((data) => self.view.exportData(data));
    }

    filterData(filter) {
        let self = this;

        self.model.findAccording("name", filter, self.page, self.rowsPerPage, (filteredData, currentPage, rowsPerPage, isFiltering) => {self.view.render(filteredData, currentPage, rowsPerPage, isFiltering); self._setDragEvents();});
    }

    paginateData(page) {
        let self = this;
        self.page = page - 1;

        this.model.getData((data) => {self.view.render(data, self.page, self.rowsPerPage); self._setDragEvents();}, true);
    }

    importData(importedData) {
        let self = this;
        self.page = 0;

        this.model.replaceData(importedData, (data) => {self.view.render(data, self.page, self.rowsPerPage); self._setDragEvents(); self.view._removeSortingHighLight();});
    }

    sortingData(sortField, sortDirection) {
        let self = this;

        this.model.sortDataByField(sortField, sortDirection, (data) => {self.view.render(data, self.page, self.rowsPerPage); self._setDragEvents();});
    }

    dragRow(startElementId, endElementId) {
        let self = this;

        this.model.dragShiftData(startElementId, endElementId, (data) => {self.view.render(data, self.page, self.rowsPerPage); self._setDragEvents();});
    }

    initApp() {
        let self = this;

        this.view.bind("createTable", () => self.tableCreating());
        this.view.bind("addNewRow", (rowData) => self.addingRow(rowData));
        this.view.bind("generateDemoData", () => self.generateData());
        this.view.bind("removeCheckedRows", (removableRowsId) => self.removeRows(removableRowsId));
        this.view.bind("removeAllRows", () => self.removeAllRows());
        this.view.bind("exportData", () => self.exportData());
        this.view.bind("importData", (importedData) => self.importData(importedData));
        this.view.bind("filterData", (filterData) => self.filterData(filterData));
        this.view.bind("paginateData", (pageNumber) => self.paginateData(pageNumber));
        this.view.bind("sortData", (data, sortDirection) => self.sortingData(data, sortDirection));
    }

    _setDragEvents() {
        this.view.bind("dragRow", (startElementId, endElementId) => this.dragRow(startElementId, endElementId))
    }

    _validateData(rowData) {
        let errorList = [];
        let rules;
        let value;
        let isError;

        for (let key in this.fieldRules) {
            rules = this.fieldRules[key];
            value = rowData[key];

            isError = false;

            for (let i = 0; i < rules.length && !isError; i++) {
                switch (rules[i]) {
                    case "not-empty":
                        if (value.toString().trim().length < 1) {
                            isError = true;
                        }
                        break;

                    case "int":
                        let regex = new RegExp("^\\d+$");

                        if (!regex.test(value)) {
                            isError = true;
                        }
                        break;

                    case "bool":
                        if (value !== "yes" && value !== "no") {
                            isError = true;
                        }
                        break;
                }
            }

            if (isError) {
                errorList.push(key);
            }
        }
        return errorList;
    }

    _getRandomValue(startPoint, endPoint) {
        return Math.round(Math.random() * (endPoint)) + startPoint;
    }

    _generateName(randomNameCharacters) {
        let nameLength = this._getRandomValue(3, 7);
        let name = "";

        for (let i=0; i < nameLength; i++) {
            name += randomNameCharacters[this._getRandomValue(0, randomNameCharacters.length - 1)];
        }

        return name;
    }
}
