let gulp = require('gulp');
let sourcemaps = require('gulp-sourcemaps');
let source = require('vinyl-source-stream');
let buffer = require('vinyl-buffer');
let browserify = require('browserify');
let watchify = require('watchify');
let babel = require('babelify');
let livereload = require('gulp-livereload');
let connect = require('gulp-connect');

function compile(watch) {
    let bundler = watchify(browserify(['./js/app.js'], {debug: true}).transform([babel, {presets: ["es2015"]}]));

    function rebundle() {
        bundler.bundle()
            .on('error', function (err) {
                console.error(err);
                this.emit('end');
            })
            .pipe(source('app.js'))
            .pipe(buffer())
            .pipe(sourcemaps.init({loadMaps: true}))
            .pipe(sourcemaps.write('./'))
            .pipe(gulp.dest('./dist'));

            gulp.src('js/app.js')
            .pipe(connect.reload());
    }

    if (watch) {
        bundler.on('update', function () {
            console.log('-> bundling...');
            rebundle();
        });
    }

    rebundle();
}

function watch() {
    return compile(true);
}

gulp.task('build', function () {
    return compile();
});
gulp.task('watch', function () {
    return watch();
});

gulp.task('connect', function() {
    connect.server({
        port: 8080,
        root: './',
        livereload: true
    });
});

gulp.task('default', ['watch', 'connect']);